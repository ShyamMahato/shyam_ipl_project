function economicalBowlerInSuperOver(deliveries) {
  const bowlerEconomy = deliveries.reduce((acc, deliveryObj) => {
    if (Number(deliveryObj.is_super_over) === 1) {
      const runsConceded =
        Number(deliveryObj.total_runs) -
        Number(deliveryObj.legbye_runs) -
        Number(deliveryObj.bye_runs);

      const wideRuns = Number(deliveryObj.wide_runs);
      const noBallRuns = Number(deliveryObj.noball_runs);

      if (acc[deliveryObj.bowler] !== undefined) {
        acc[deliveryObj.bowler]["runsConceded"] += runsConceded;

        if (wideRuns === 0 && noBallRuns === 0) {
          acc[deliveryObj.bowler]["bowlBowled"] += 1;
        }
      } else {
        acc[deliveryObj.bowler] = {};

        if (wideRuns === 0 && noBallRuns === 0) {
          acc[deliveryObj.bowler]["bowlBowled"] = 1;
        } else {
          acc[deliveryObj.bowler]["bowlBowled"] = 0;
        }

        acc[deliveryObj.bowler]["runsConceded"] = runsConceded;
      }
    }

    return acc;
  }, {});

  const economyRates = [];

  Object.keys(bowlerEconomy).map((bowler) => {
    const economy = (
      bowlerEconomy[bowler].runsConceded /
      (bowlerEconomy[bowler].bowlBowled / 6)
    ).toFixed(2);

    economyRates.push([bowler, economy]);
  });

  economyRates.sort((bowler1, bowler2) => {
    return bowler1[1] - bowler2[1];
  });

  return economyRates.reduce((acc, bowler) => {
    const bowlerName = bowler[0];
    const economyRate = bowler[1];
    acc[bowlerName] = economyRate;

    return acc;
  }, {});
}

module.exports = economicalBowlerInSuperOver;

function strikeRateOfBatsman(matches, deliveries) {
  const batsmanStrikeRate = deliveries.reduce((deliveryAcc, deliveryObj) => {
    matches.map((matchObj) => {
      const season = matchObj.season;

      if (matchObj.id === deliveryObj.match_id) {
        if (deliveryAcc[season] !== undefined) {
          if (deliveryAcc[season][deliveryObj.batsman] !== undefined) {
            deliveryAcc[season][deliveryObj.batsman]["runs"] += Number(
              deliveryObj.batsman_runs
            );

            if (Number(deliveryObj.wide_runs) === 0) {
              deliveryAcc[season][deliveryObj.batsman]["ballFaced"] += 1;
            }
          } else {
            deliveryAcc[season][deliveryObj.batsman] = {};
            deliveryAcc[season][deliveryObj.batsman]["runs"] = Number(
              deliveryObj.batsman_runs
            );

            if (Number(deliveryObj.wide_runs) === 0) {
              deliveryAcc[season][deliveryObj.batsman]["ballFaced"] = 1;
            } else {
              deliveryAcc[season][deliveryObj.batsman]["ballFaced"] = 0;
            }
          }
        } else {
          deliveryAcc[season] = {};
          deliveryAcc[season][deliveryObj.batsman] = {};

          deliveryAcc[season][deliveryObj.batsman]["runs"] = Number(
            deliveryObj.batsman_runs
          );

          if (Number(deliveryObj.wide_runs) === 0) {
            deliveryAcc[season][deliveryObj.batsman]["ballFaced"] = 1;
          } else {
            deliveryAcc[season][deliveryObj.batsman]["ballFaced"] = 0;
          }
        }
      }
    });

    return deliveryAcc;
  }, {});

  let batsmanBattingRecord = [];

  return Object.keys(batsmanStrikeRate).reduce((acc, season) => {
    batsmanBattingRecord = [];

    Object.keys(batsmanStrikeRate[season]).map((batsman) => {
      const strikeRate = (
        (batsmanStrikeRate[season][batsman].runs /
          batsmanStrikeRate[season][batsman].ballFaced) *
        100
      ).toFixed(2);

      if (acc[season] !== undefined) {
        acc[season][batsman] = strikeRate;
      } else {
        acc[season] = {};
        acc[season][batsman] = strikeRate;
      }
    });

    return acc;
  }, {});
}

module.exports = strikeRateOfBatsman;

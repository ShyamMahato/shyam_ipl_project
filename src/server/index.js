const fs = require("fs");
const path = require("path");
const csv = require("csvtojson");

const matchesPlayedPerYear = require("../ipl/matchesPlayedPerYear");
const matchesWonPerYear = require("../ipl/matchesWonPerYear");
const extraRunConcededPerTeam = require("../ipl/extraRunConcededPerTeam");
const topTenEconomicalBowler = require("../ipl/topTenEconomicalBowler");
const teamWonTossAndMatch = require("../ipl/teamWonTossAndMatch");
const maximumPlayerOfMatchAward = require("../ipl/maximumPlayerOfMatchAward");
const strikeRateOfBatsman = require("../ipl/strikeRateOfBatsman");
const playerDismissedByPlayer = require("../ipl/playerDismissedByPlayer");
const bestEconomyInSuperOver = require("../ipl/economicalBowlerInSuperOver");

const MATCHES_FILE_PATH = path.join(__dirname, "../data/matches.csv");
const DELIVERIES_FILE_PATH = path.join(__dirname, "../data/deliveries.csv");

const JSON_OUTPUT_FILE_PATH = "../public/output/";

const JSON_OUTPUT_FILE_NAMES = [
  "matchesPlayedPerYear.json",
  "matchesWonPerYear.json",
  "extraRunConcededPerTeam.json",
  "topEconomicalBowler.json",
  "teamWonTossAndMatch.json",
  "maximumPlayerOfMatchAward.json",
  "strikeRateOfBatsman.json",
  "playerDismissedByPlayer.json",
  "bestEconomyInSuperOver.json",
];

function main(dataArr) {
  const matches = dataArr[0];
  const deliveries = dataArr[1];

  const matchesPlayed = matchesPlayedPerYear(matches);
  const matchesWon = matchesWonPerYear(matches);
  const tossAndMatchWin = teamWonTossAndMatch(matches);
  const playerOfMatchAward = maximumPlayerOfMatchAward(matches);

  const extraRunConceded = extraRunConcededPerTeam(matches, deliveries, 2016);
  const economicalBowler = topTenEconomicalBowler(matches, deliveries, 2015);
  const strikeRate = strikeRateOfBatsman(matches, deliveries);

  const playerDismissed = playerDismissedByPlayer(deliveries);
  const economicalBowlerInSuperOver = bestEconomyInSuperOver(deliveries);

  const results = [
    matchesPlayed,
    matchesWon,
    extraRunConceded,
    economicalBowler,
    tossAndMatchWin,
    playerOfMatchAward,
    strikeRate,
    playerDismissed,
    economicalBowlerInSuperOver,
  ];

  const resultPromises = [];

  results.map((result, index) => {
    const jsonString = JSON.stringify(result);
    resultPromises.push(saveResults(JSON_OUTPUT_FILE_NAMES[index], jsonString));
  });

  Promise.all(resultPromises)
    .then((successArr) => {
      successArr.map((success) => console.log(success));
    })
    .catch((err) => console.error(err));

  return;
}

function saveResults(filename, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(
      path.join(__dirname, JSON_OUTPUT_FILE_PATH, filename),
      data,
      "utf-8",
      (err) => {
        if (err) {
          reject(err);
        } else {
          resolve("File dumped successfully.");
        }
      }
    );
  });
}

function readCsvData(matchFile, deliveryFile) {
  const promises = [];

  promises.push(csv().fromFile(matchFile), csv().fromFile(deliveryFile));

  Promise.all(promises)
    .then((resultArr) => {
      main(resultArr);
    })
    .catch((err) => {
      console.error(err);
    });

  return;
}

readCsvData(MATCHES_FILE_PATH, DELIVERIES_FILE_PATH);

function matchesPlayedPerYear(matches) {
  return matches.reduce((acc, matchObj) => {
    if (acc[matchObj.season] !== undefined) {
      acc[matchObj.season] += 1;
    } else {
      acc[matchObj.season] = 1;
    }

    return acc;
  }, {});
}

module.exports = matchesPlayedPerYear;

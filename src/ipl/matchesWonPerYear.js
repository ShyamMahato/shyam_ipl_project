function matchesWonPerYear(matches) {
  return matches.reduce((acc, matchObj) => {
    if (acc[matchObj.season] !== undefined) {
      if (acc[matchObj.season][matchObj.winner] !== undefined) {
        acc[matchObj.season][matchObj.winner] += 1;
      } else {
        acc[matchObj.season][matchObj.winner] = 1;
      }
    } else {
      acc[matchObj.season] = {};
      acc[matchObj.season][matchObj.winner] = 1;
    }

    return acc;
  }, {});
}

module.exports = matchesWonPerYear;

function teamWonTossAndMatch(matches) {
  return matches.reduce((acc, matchObj) => {
    if (matchObj.toss_winner === matchObj.winner) {
      if (acc[matchObj.winner] !== undefined) {
        acc[matchObj.winner] += 1;
      } else {
        acc[matchObj.winner] = 1;
      }
    }

    return acc;
  }, {});
}

module.exports = teamWonTossAndMatch;

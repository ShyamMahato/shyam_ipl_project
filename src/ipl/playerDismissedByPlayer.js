function playerDismissedByPlayer(deliveries) {
  const playerDismissal = deliveries.reduce((acc, deliveryObj) => {
    if (
      deliveryObj.player_dismissed !== "" &&
      deliveryObj.dismissal_kind !== "run out" &&
      deliveryObj.dismissal_kind !== "obstructing the field" &&
      deliveryObj.dismissal_kind !== "retired hurt"
    ) {
      if (acc[deliveryObj.player_dismissed] !== undefined) {
        if (acc[deliveryObj.player_dismissed][deliveryObj.bowler] !== undefined) {
          acc[deliveryObj.player_dismissed][deliveryObj.bowler] += 1;
        } else {
          acc[deliveryObj.player_dismissed][deliveryObj.bowler] = 1;
        }
      } else {
        acc[deliveryObj.player_dismissed] = {};
        acc[deliveryObj.player_dismissed][deliveryObj.bowler] = 1;
      }
    }

    return acc;
  }, {});

  let playerRecord = [];

  return Object.keys(playerDismissal).reduce((acc, batsman) => {
    playerRecord = [];

    Object.keys(playerDismissal[batsman]).map((bowler) => {
      playerRecord.push([bowler, playerDismissal[batsman][bowler]]);
    });

    playerRecord
      .sort((player1, player2) => {
        return player2[1] - player1[1];
      })
      .slice(0, 10);

    const bowlerName = playerRecord[0][0];
    const outNumberOfTime = playerRecord[0][1];

    acc[batsman] = {};
    acc[batsman][bowlerName] = outNumberOfTime;

    return acc;
  }, {});
}

module.exports = playerDismissedByPlayer;

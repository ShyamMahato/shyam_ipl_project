function extraRunConcededPerTeam(matches, deliveries, year = 2016) {
  const matchIds = matches
    .filter((match) => match.season == year)
    .map((matchObj) => {
      return matchObj.id;
    });

  return deliveries.reduce((acc, deliveryObj) => {
    if (matchIds.indexOf(deliveryObj.match_id) !== -1) {
      if (acc[deliveryObj.bowling_team]) {
        acc[deliveryObj.bowling_team] += Number(deliveryObj.extra_runs);
      } else {
        acc[deliveryObj.bowling_team] = Number(deliveryObj.extra_runs);
      }
    }

    return acc;
  }, {});
}

module.exports = extraRunConcededPerTeam;

function maximumPlayerOfMatchAward(matches) {
  const playerOfMatchRecord = matches.reduce((acc, matchObj) => {
    if (acc[matchObj.season] !== undefined) {
      if (acc[matchObj.season][matchObj.player_of_match] !== undefined) {
        acc[matchObj.season][matchObj.player_of_match] += 1;
      } else {
        acc[matchObj.season][matchObj.player_of_match] = 1;
      }
    } else {
      acc[matchObj.season] = {};
      acc[matchObj.season][matchObj.player_of_match] = 1;
    }

    return acc;
  }, {});

  let playersWithAwards = [];

  return Object.keys(playerOfMatchRecord).reduce((acc, season) => {
    playersWithAwards = [];

    Object.keys(playerOfMatchRecord[season]).map((player) => {
      playersWithAwards.push([player, playerOfMatchRecord[season][player]]);
    });

    playersWithAwards.sort((player1, player2) => {
      return player2[1] - player1[1];
    });

    const playerWithMaxAward = playersWithAwards[0][0];
    const playerNumberOfAward = playersWithAwards[0][1];

    acc[season] = {};
    acc[season][playerWithMaxAward] = playerNumberOfAward;

    return acc;
  }, {});
}

module.exports = maximumPlayerOfMatchAward;

function topEconomicalBowler(matches, deliveries, year = 2015) {
  const matchIds = matches
    .filter((match) => match.season == year)
    .map((matchObj) => matchObj.id);

  const bowlerStat = deliveries.reduce((acc, deliveryObj) => {
    if (matchIds.indexOf(deliveryObj.match_id) !== -1) {
      const runsConceded =
        Number(deliveryObj.total_runs) -
        Number(deliveryObj.legbye_runs) -
        Number(deliveryObj.bye_runs);

      const wideRuns = Number(deliveryObj.wide_runs);
      const noBallRuns = Number(deliveryObj.noball_runs);

      if (acc[deliveryObj.bowler] !== undefined) {
        acc[deliveryObj.bowler]["runsConceded"] += runsConceded;

        if (wideRuns === 0 && noBallRuns === 0) {
          acc[deliveryObj.bowler]["bowlBowled"] += 1;
        }
      } else {
        acc[deliveryObj.bowler] = {};

        if (wideRuns === 0 && noBallRuns === 0) {
          acc[deliveryObj.bowler]["bowlBowled"] = 1;
        } else {
          acc[deliveryObj.bowler]["bowlBowled"] = 0;
        }

        acc[deliveryObj.bowler]["runsConceded"] = runsConceded;
      }
    }

    return acc;
  }, {});

  const bowlerEconomy = [];

  Object.keys(bowlerStat).map((bowler) => {
    const economy = (
      bowlerStat[bowler].runsConceded /
      (bowlerStat[bowler].bowlBowled / 6)
    ).toFixed(2);

    bowlerEconomy.push([bowler, economy]);
  });

  return bowlerEconomy
    .sort((player1, player2) => {
      return player1[1] - player2[1];
    })
    .slice(0, 10)
    .reduce((acc, bowler) => {
      const bowlerName = bowler[0];
      const bowlerEconomyRate = bowler[1];

      acc[bowlerName] = bowlerEconomyRate;

      return acc;
    }, {});
}

module.exports = topEconomicalBowler;
